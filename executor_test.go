package executor

import (
	"errors"
	"testing"
	"time"
)

func TestConcurrency(t *testing.T) {
	// total sleep time == 250 msec
	// expect actual execution time will be close to slowest operation
	tasks := []Task{
		func() error {
			time.Sleep(25 * time.Millisecond)
			return nil
		},
		func() error {
			time.Sleep(50 * time.Millisecond)
			return nil
		},
		func() error {
			time.Sleep(75 * time.Millisecond)
			return nil
		},
		func() error {
			time.Sleep(100 * time.Millisecond)
			return nil
		},
	}
	start := time.Now()
	_ = Execute(4, 0, tasks...)
	end := time.Since(start)
	maxExpectedTime := 110 * time.Millisecond
	if end > maxExpectedTime {
		t.Error("Execution takes too long time")
	}
}

func TestSemaphore(t *testing.T) {
	// sequential execution (concurrency == 1)
	// expect actual execution time will be greater than total execution time
	tasks := []Task{
		func() error {
			time.Sleep(25 * time.Millisecond)
			return nil
		},
		func() error {
			time.Sleep(25 * time.Millisecond)
			return nil
		},
		func() error {
			time.Sleep(25 * time.Millisecond)
			return nil
		},
		func() error {
			time.Sleep(25 * time.Millisecond)
			return nil
		},
	}
	start := time.Now()
	_ = Execute(1, 0, tasks...)
	end := time.Since(start)
	minExpectedTime := 100 * time.Millisecond
	if end < minExpectedTime {
		t.Error("Execution takes too short time")
	}
}

func TestTermination(t *testing.T) {
	// expect actual execution time will be close to slowest (broken) operation
	tasks := []Task{
		func() error {
			time.Sleep(250 * time.Millisecond)
			return errors.New("task 1 error")
		},
		func() error {
			time.Sleep(300 * time.Millisecond)
			return nil
		},
		func() error {
			time.Sleep(300 * time.Millisecond)
			return nil
		},
		func() error {
			time.Sleep(300 * time.Millisecond)
			return nil
		},
	}
	start := time.Now()
	err := Execute(1, 0, tasks...)
	if err == nil {
		t.Error("Error must not be nil")
	}
	end := time.Since(start)
	maxExpectedTime := 299 * time.Millisecond
	if end > maxExpectedTime {
		t.Error("Execution takes too long time")
	}
}
