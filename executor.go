package executor

import (
	"errors"
	"fmt"
)

type Task func() error

func Execute(concurrency int, errorsLimit int, tasks ...Task) error {

	if concurrency <= 0 {
		return errors.New("concurrency level must be >= 1")
	}

	// канал для ошибок / результатов
	errorsCh := make(chan error)
	// канал для отправки задач воркерам
	tasksCh := make(chan Task)
	// канал для сигнала отмены
	cancelCh := make(chan struct{})

	// создаем пул воркеров
	for i := 0; i < concurrency; i++ {
		go func() {
			for task := range tasksCh {
				errorsCh <- task()
			}
		}()
	}

	// создаем поставщика задач
	go func() {
		for _, task := range tasks {
			select {
			case <-cancelCh:
				// все каналы синхронные, поэтому cancelCh не проверяется в воркерах
				close(tasksCh)
				return
			default:
				tasksCh <- task
			}
		}
		close(tasksCh)
	}()

	errorsCounter := 0
	for i := 0; i < len(tasks); i++ {
		if err := <-errorsCh; err != nil {
			errorsCounter++
		}
		if errorsCounter > errorsLimit {
			close(cancelCh)
			return fmt.Errorf("too many errors: %d happened %d allowed", errorsCounter, errorsLimit)
		}
	}
	return nil
}
